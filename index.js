/**
 * node-ibe - Identity-based encryption for node.
 *
 * This code is based off of IBE code created by Daniel Cheng <dongxucheng@gmail.com>.
 *
 * The MIT License (MIT)
 * Copyright (c) 2015 Internet Security Research Lab.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @module
 */

var _      = require('lodash'),
    BigInt = require('BigInt'),
    crypto = require('crypto');

/**
 * Converts a string into a BigInt.
 * @param {string} str String value.
 * @returns {BigInt} BigInt value.
 */
var str2bigInt = function (str) {
  return BigInt.str2bigInt(str, 10, 0);
};

/**
 * Array of strings to field extension.
 * @param {string[]} x The string array representation of the field extension value.
 * @returns {BigInt[]} The field extension value.
 */
var strArray2fe = function (x) {
  return x.map(str2bigInt);
};

var zero  = BigInt.int2bigInt(0, 1, 1),
    one   = BigInt.int2bigInt(1, 2, 1),
    two   = BigInt.int2bigInt(2, 2, 1),
    three = BigInt.int2bigInt(3, 2, 1),
    five  = BigInt.int2bigInt(5, 2, 1);

var defaultKeyParams = {
  secret:    str2bigInt("10681007545619528139211807714513200280572961850194771052062491085370"),
  systemPublicKey: [
    strArray2fe([
      "8733743139883721929746557669281786776630567016143461292221315656826",
      "8361394066264508648455507973062910774845860829356344049728407408780",
      "10875262355316927425360895615220150789510813104233695541722171619177"
    ]),
    strArray2fe([
      "10447339307901630518074865526175395285504103515763147836036408151730",
      "5624160785336662084843147096952829593564757784171941592985688526173",
      "3410094274429991793295974722413474163646214393604192093427842333867"
    ])
  ]
};

var ecMultiplier = str2bigInt("1871224163624666631860092489128939059944978347142292177323825642096"),
    lookupSize   = 16,
    modulus      = str2bigInt('15028799613985034465755506450771565229282832217860390155996483840017'),
    nqrx         = [str2bigInt("142721363302176037340346936780070353538541593770301992936740616924"), zero, zero],
    generator    = [
      strArray2fe([
        "13811081979843030123575683961364195098182780905035434741449961170701",
        "5168311754219690299065397857137804743287622694314500894713928555910",
        "8811934331084896044063754515309978334585216131609973100543622590549"
      ]),
      strArray2fe([
        "11978937640305627329722610355100010500665621620036973046712737173409",
        "4386115112854559530338202238348812530267878704872566762635368727761",
        "10391044986192973973988118337245431673037549976707601333607548174896"
      ])
    ];

/// --------------------------------------------------------------------------------------------------------------------
/// Modular arithmatic
/// --------------------------------------------------------------------------------------------------------------------

/**
 * Calculate -x (mod modulus).
 * @param {BigInt} x
 * @returns {BigInt}
 */
var modNeg = function (x) {
  if (BigInt.isZero(x)) {
    return BigInt.dup(x);
  } else {
    return BigInt.sub(modulus, x);
  }
};

/**
 * Calculate x + y (mod modulus).
 * @param {BigInt} x
 * @param {BigInt} y
 * @returns {BigInt}
 */
var modAdd = function (x, y) {
  var s = BigInt.add(x, y);
  if (BigInt.greater(s, modulus)) {
    s = BigInt.sub(s, modulus);
  }
  return s;
};

/**
 * Calculate x - y (mod modulus).
 * @param {BigInt} x
 * @param {BigInt} y
 * @returns {BigInt}
 */
var modSub = function (x, y) {
  if (BigInt.greater(y, x)) {
    return BigInt.sub(BigInt.add(x, modulus), y);
  } else {
    return BigInt.sub(x, y);
  }
};

/**
 * Calculate x * y (mod modulus).
 * @param {BigInt} x
 * @param {BigInt} y
 * @returns {BigInt}
 */
var modMul = function (x, y) {
  return BigInt.multMod(x, y, modulus);
};

/**
 * Calculate x^y (mod modulus).
 * @param {BigInt} x
 * @param {BigInt} y
 * @returns {BigInt}
 */
var modPow = function (x, y) {
  return BigInt.powMod(x, y, modulus);
};

/**
 * Calculate x^(-1) (mod modulus).
 * @param {BigInt} x
 * @returns {BigInt}
 */
var modInverse = function (x) {
  return BigInt.inverseMod(x, modulus);
};

/**
 * Calculate (a/n).
 * @param {BigInt} a
 * @param {BigInt} n
 * @returns {number} 1 if a may be a square mod n, -1 if it is not, or 0 if a = n (mod n).
 */
var jacobi = function (a, n) {
  if (BigInt.isZero(a)) {
    return 0;
  }
  if (BigInt.equalsInt(a, 1)) {
    return 1;
  }
  if (BigInt.equalsInt(a, 2)) {
    var n8 = BigInt.modInt(n, 8);
    if (n8 == 3 || n8 == 5) {
      return -1;
    }
    else {
      return 1;
    }
  }

  var remainder, divisor;
  if (BigInt.modInt(a, 2) == 0) {
    divisor = BigInt.dup(a);
    remainder = BigInt.dup(a);
    BigInt.divide_(a, two, divisor, remainder);
    return jacobi(two, n) * jacobi(divisor, n);
  }

  if (BigInt.greater(a, n)) {
    divisor = BigInt.dup(a);
    remainder = BigInt.dup(a);
    BigInt.divide_(a, n, divisor, remainder);
    return jacobi(remainder, n);
  }

  if (BigInt.modInt(a, 4) == 3 && BigInt.modInt(n, 4) == 3) {
    return -1 * jacobi(n, a);
  }
  else {
    return jacobi(n, a);
  }
};

/**
 * Calculate x^(1/2) (mod modulus).
 * @param {BigInt} x
 * @returns {BigInt}
 */
var modSqrt = function (x) {
  if (BigInt.isZero(x)) {
    return zero;
  }

  x = modPow(x, modSqrt.sp1d4);
  var b = modPow(x, modSqrt.s);
  var g = modPow(five, modSqrt.s);
  var r = 4;
  while (true) {
    var t = BigInt.dup(b);
    var m = 0;
    for (; m < r; ++m) {
      if (BigInt.equalsInt(t, 1)) {
        break;
      }
      t = modMul(t, t);
    }
    if (m == 0) {
      return x
    }

    var rmm1 = modPow(two, BigInt.int2bigInt(r - m - 1, 4, 1));
    var gs = modPow(g, rmm1);
    g = modMul(gs, gs);
    x = modMul(x, gs);
    b = modMul(b, g);
    r = m;
  }
};

modSqrt.sp1d4 = str2bigInt("469649987937032327054859576586611413415088506808137192374890120001");
modSqrt.s = str2bigInt("939299975874064654109719153173222826830177013616274384749780240001");

/// --------------------------------------------------------------------------------------------------------------------
/// Field extension functions
/// --------------------------------------------------------------------------------------------------------------------

/**
 * Duplicate x.
 * @param {BigInt[]} x
 * @returns {BigInt[]}
 */
var feDup = function (x) {
  return x.map(BigInt.dup);
};

/**
 * Calculate -x.
 * @param {BigInt[]} x
 * @returns {BigInt[]}
 */
var feNeg = function (x) {
  return x.map(modNeg);
};

/**
 * Calculate x + y.
 * @param {BigInt[]} x
 * @param {BigInt[]} y
 * @returns {BigInt[]}
 */
var feAdd = function (x, y) {
  return _.zipWith(x, y, modAdd);
};

/**
 * Calculate x - y.
 * @param {BigInt[]} x
 * @param {BigInt[]} y
 * @returns {BigInt[]}
 */
var feSub = function (x, y) {
  return _.zipWith(x, y, modSub);
};

/**
 * Calculate x * y.
 * @param {BigInt} x
 * @param {BigInt[]} y
 * @returns {BigInt[]}
 */
var feMulCons = function (x, y) {
  return y.map(function (val) {
    return modMul(x, val);
  });
};

/**
 * Calculate x * y.
 * @param {BigInt[]} x
 * @param {BigInt[]} y
 * @returns {BigInt[]}
 */
var feMul = function (x, y) {
  var dest    = new Array(3),
      scratch = new Array(3),
      temp1, temp2;

  temp1 = modAdd(x[0], x[1]);
  temp2 = modAdd(y[0], y[1]);
  scratch[2] = modMul(temp1, temp2);

  temp1 = modAdd(x[0], x[2]);
  temp2 = modAdd(y[0], y[2]);
  scratch[1] = modMul(temp1, temp2);

  temp1 = modAdd(x[1], x[2]);
  temp2 = modAdd(y[1], y[2]);
  scratch[0] = modMul(temp1, temp2);

  dest[1] = modMul(x[1], y[1]);
  dest[0] = modMul(x[0], y[0]);

  temp2 = modMul(x[2], y[2]);
  temp1 = modAdd(dest[1], temp2);
  temp1 = modSub(scratch[0], temp1);

  dest[2] = modAdd(dest[0], temp2);
  scratch[1] = modSub(scratch[1], dest[2]);
  dest[2] = modAdd(dest[1], scratch[1]);

  scratch[2] = modSub(scratch[2], dest[0]);
  dest[1] = modSub(scratch[2], dest[1]);

  dest = feAdd(dest, feMulCons(temp1, feMul.xpwr));
  return feAdd(dest, feMulCons(temp2, feMul.xpwr1));
};
feMul.xpwr = strArray2fe([
  "3053610355725337299498468625544028297836124273177919204884624393825",
  "1595757413637099638012768355522018425276144655772136098584582477246",
  "6701335092867243227676401275323443222522968592352346660225596428403"
]);
feMul.xpwr1 = strArray2fe([
  "9385910079666998837875568805382738923761376323975517375834674752474",
  "12727029633817405504618036300927848741686155698560986497611454569413",
  "11293357710551192191735297347007133101291849005436098117914318596946"
]);

/**
 * Calculate x == y.
 * @param {BigInt[]} x
 * @param {BigInt[]} y
 * @returns {boolean}
 */
var feEquals = function (x, y) {
  return _.all(_.zipWith(x, y, BigInt.equals));
};

/**
 * Calculate x == 0.
 * @param {BigInt[]} x
 * @returns {boolean}
 */
var feIsZero = function (x) {
  return _.all(x.map(BigInt.isZero));
};

/// --------------------------------------------------------------------------------------------------------------------
/// Polynomial functions
/// --------------------------------------------------------------------------------------------------------------------

/**
 * Trim the polynomial to the non-zero entries.
 * @param {BigInt[]} x
 * @return {BigInt[]}
 */
var polyTrim = function (x) {
  var i = x.length - 1;
  for (; i >= 0 && BigInt.isZero(x[i]); i--) {}
  return x.slice(0, i + 1);
};

/**
 * Calculate x - y.
 * @param {BigInt[]} x
 * @param {BigInt[]} y
 * @returns {BigInt[]}
 */
var polySub = function (x, y) {
  var length = x.length < y.length ? x.length : y.length,
      result = new Array(length),
      i;

  for (i = 0; i < length; i++) {
    result[i] = modSub(x[i], y[i]);
  }

  for (; i < x.length; i++) {
    result[i] = modSub(x[i], zero);
  }

  for (; i < y.length; i++) {
    result[i] = modSub(zero, y[i]);
  }

  return result;
};

/**
 * Calculate x * y.
 * @param {BigInt[]} x
 * @param {BigInt[]} y
 * @returns {BigInt[]}
 */
var polyMul = function (x, y) {
  if (x.length == 0 || y.length == 0) {
    return [];
  }

  var xLength = x.length,
      yLength = y.length,
      n       = xLength + yLength - 1,
      r       = new Array(n);

  for (var i = 0; i < n; ++i) {
    r[i] = BigInt.dup(zero);
    for (var j = 0; j <= i; ++j) {
      if (j < xLength && i - j < yLength) {
        r[i] = modAdd(r[i], modMul(x[j], y[i - j]));
      }
    }
  }

  return polyTrim(r);
};

/**
 * Calculate x / y.
 * @param {BigInt[]} x
 * @param {BigInt[]} y
 * @returns {{quotient: BigInt[], remainder: BigInt[]}}
 */
var polyDiv = function (x, y) {
  if (y.length > x.length) {
    return [[], x];
  }

  var n    = y.length - 1,
      m    = x.length - 1,
      k    = m - n,
      pr   = feDup(x),
      pq   = new Array(k + 1),
      binv = modInverse(y[n]);

  for (; k >= 0; k--, m--) {
    pq[k] = modMul(binv, pr[m]);
    for (var i = 0; i <= n; i++) {
      pr[i + k] = modSub(pr[i + k], modMul(pq[k], y[i]))
    }
  }

  return {
    quotient:  pq,
    remainder: polyTrim(pr)
  };
};

/**
 * Calculate x^(-1) (mod polyInverse.modulus).
 * @param {BigInt[]} x
 * @returns {BigInt[]}
 */
var polyInverse = function (x) {
  var r0 = polyInverse.modulus,
      r1 = x,
      b0 = [],
      b1 = [one];

  while (true) {
    var result = polyDiv(r0, r1);
    if (result.remainder.length == 0) {
      return feMulCons(modInverse(r1[0]), b1);
    }

    var temp = polySub(b0, polyMul(b1, result.quotient));
    b0 = b1;
    b1 = temp;
    r0 = r1;
    r1 = result.remainder;
  }
};
polyInverse.modulus = strArray2fe([
  "11975189258259697166257037825227536931446707944682470951111859446192",
  "13433042200347934827742738095249546804006687562088254057411901362771",
  "8327464521117791238079105175448122006759863625508043495770887411614",
  "1"
]);

/**
 * Calculate x == y.
 * @param {BigInt[]} x
 * @param {number} y
 * @returns {boolean}
 */
var polyEqualsInt = function (x, y) {
  if (!BigInt.equalsInt(x[0], y)) {
    return false;
  }

  for (var i = 1; i < x.length; i++) {
    if (!BigInt.isZero(x[i])) {
      return false;
    }
  }

  return true;
};

/// --------------------------------------------------------------------------------------------------------------------
/// Elliptic curve functions
/// --------------------------------------------------------------------------------------------------------------------

/**
 * Compuet P + P.
 * @param {BigInt[]} P
 * @returns {BigInt[]}
 */
var ecDouble = function (P) {
  if (BigInt.equalsInt(P[1], 0)) {
    return [[], zero];
  }

  var x = P[0];
  var y = P[1];

  var lambda = modMul(x, x);
  lambda = modMul(lambda, three);
  lambda = modAdd(lambda, ecMultiplier);

  var e0 = modAdd(y, y);
  e0 = BigInt.inverseMod(e0, modulus);
  lambda = modMul(lambda, e0);

  var e1 = modAdd(x, x);
  e0 = modMul(lambda, lambda);
  e0 = modSub(e0, e1);
  e1 = modSub(x, e0);
  e1 = modMul(e1, lambda);
  e1 = modSub(e1, y);

  return [e0, e1];
};

/**
 * Compuet P + Q.
 * @param {BigInt[]} P
 * @param {BigInt[]} Q
 * @returns {BigInt[]}
 */
var ecAdd = function (P, Q) {
  if (ecIsZero(P)) {
    return Q;
  }
  if (ecIsZero(Q)) {
    return P;
  }

  if (BigInt.equals(P[0], Q[0])) {
    if (BigInt.equals(P[1], Q[1]) && !BigInt.isZero(P[1])) {
      return ecDouble(P);
    }
    return [zero, zero];
  }

  var e0 = modSub(Q[0], P[0]);
  e0 = BigInt.inverseMod(e0, modulus);

  var lambda = modSub(Q[1], P[1]);
  lambda = modMul(lambda, e0);
  e0 = modMul(lambda, lambda);
  e0 = modSub(e0, P[0]);
  e0 = modSub(e0, Q[0]);

  var e1 = modSub(P[0], e0);
  e1 = modMul(e1, lambda);
  e1 = modSub(e1, P[1]);

  return [e0, e1];
};

/**
 * Calculate x == 0.
 * @param {BigInt[]} x
 * @returns {boolean}
 */
var ecIsZero = feIsZero;

/**
 * Compuet P + P.
 * @param {BigInt[][]} P
 * @returns {BigInt[][]}
 */
var ecFeDouble = function (P) {
  if (feIsZero(P[0]) && feIsZero(P[1])) {
    return P;
  }

  var lambda = feMul(P[0], P[0]);
  lambda = feMulCons(three, lambda);
  lambda = feAdd(lambda, ecFeDouble.a);

  var e0 = feAdd(P[1], P[1]);
  e0 = polyInverse(e0);
  lambda = feMul(lambda, e0);

  var e1 = feAdd(P[0], P[0]);
  e0 = feMul(lambda, lambda);
  e0 = feSub(e0, e1);
  e1 = feSub(P[0], e0);
  e1 = feMul(e1, lambda);
  e1 = feSub(e1, P[1]);

  return [e0, e1];
};
ecFeDouble.a = [str2bigInt("814666024090928804996017557078651267356185679871471934426494531496"), zero, zero];

/**
 * Compuet P + Q.
 * @param {BigInt[][]} P
 * @param {BigInt[][]} Q
 * @returns {BigInt[][]}
 */
var ecFeAdd = function (P, Q) {
  if (feIsZero(P[0]) && feIsZero(P[1])) {
    return Q;
  }

  if (feEquals(P[0], Q[0]) && feEquals(P[1], Q[1])) {
    return ecFeDouble(P);
  }

  var e0 = feSub(Q[0], P[0]);
  e0 = polyInverse(e0);

  var lambda = feSub(Q[1], P[1]);
  lambda = feMul(lambda, e0);
  e0 = feMul(lambda, lambda);
  e0 = feSub(e0, P[0]);
  e0 = feSub(e0, Q[0]);

  var e1 = feSub(P[0], e0);
  e1 = feMul(e1, lambda);
  e1 = feSub(e1, P[1]);

  return [e0, e1];
};

/// --------------------------------------------------------------------------------------------------------------------
/// IBE methods
/// --------------------------------------------------------------------------------------------------------------------

var fqMul = function (P, Q) {
  var e0 = feMul(P[0], Q[0]),
      e1 = feMul(P[1], Q[1]),
      e2 = feSub(feMul(feAdd(P[0], P[1]), feAdd(Q[0], Q[1])), e0);

  return [feAdd(feMul(e1, nqrx), e0), feSub(e2, e1)];
};

var fqInvert = function (P) {
  var e0 = feMul(P[0], P[0]),
      e1 = feMul(P[1], P[1]);

  e1 = feMul(e1, nqrx);
  e0 = feSub(e0, e1);
  e0 = polyInverse(e0);

  return [feMul(P[0], e0), feMul(P[1], feNeg(e0))];
};

var dMillerEval = function (a, b, c, Qx, Qy) {
  var e0 = [new Array(3), new Array(3)];
  for (var i = 0; i < 3; ++i) {
    e0[0][i] = modMul(Qx[i], a);
    e0[1][i] = modMul(Qy[i], b);
  }
  e0[0][0] = modAdd(e0[0][0], c);
  return e0;
};

var lucasEven = function (P, cofactor) {
  if (polyEqualsInt(P[0], 0) && polyEqualsInt(P[1], 0)) {
    return P;
  }

  var t0    = [two, zero, zero],
      t1    = feAdd(P[0], P[0]),
      out   = [t0, t1],
      bytes = BigInt.bigInt2str(cofactor, 2);

  for (var j = bytes.length - 1; j > 0; j--) {
    if (bytes.charAt(bytes.length - j - 1) == '1') {
      out[0] = feMul(out[0], out[1]);
      out[0] = feSub(out[0], t1);
      out[1] = feMul(out[1], out[1]);
      out[1] = feSub(out[1], t0);
    }
    else {
      out[1] = feMul(out[0], out[1]);
      out[1] = feSub(out[1], t1);
      out[0] = feMul(out[0], out[0]);
      out[0] = feSub(out[0], t0);
    }
  }

  out[1] = feMul(out[0], out[1]);
  out[1] = feSub(out[1], t1);
  out[0] = feMul(out[0], out[0]);
  out[0] = feSub(out[0], t0);
  out[0] = feAdd(out[0], out[0]);

  var t3 = feMul(t1, out[1]);
  t3 = feSub(t3, out[0]);

  t1 = feMul(t1, t1);
  t1 = feSub(t1, t0);
  t1 = feSub(t1, t0);

  out[0] = feMul(out[1], lucasEven.polyHalf);
  out[1] = feMul(t3, polyInverse(t1));
  out[1] = feMul(out[1], P[1]);

  return out;
};
lucasEven.polyHalf = [str2bigInt("7514399806992517232877753225385782614641416108930195077998241920009"), zero, zero];

var tatePower = function (input) {
  var e0 = new Array(2);

  e0[0] = feAdd(feMulCons(input[0][1], tatePower.xpowq), feMulCons(input[0][2], tatePower.xpowq2));
  e0[0][0] = modAdd(e0[0][0], input[0][0]);
  e0[1] = feAdd(feMulCons(input[1][1], tatePower.xpowq), feMulCons(input[1][2], tatePower.xpowq2));
  e0[1][0] = modAdd(e0[1][0], input[1][0]);

  var e1 = [e0[0], e0[1]];
  e0 = [input[0], feNeg(input[1])];
  e1 = fqMul(e1, e0);

  e0[0] = feAdd(feMulCons(input[0][1], tatePower.xpowq), feMulCons(input[0][2], tatePower.xpowq2));
  e0[0][0] = modAdd(e0[0][0], input[0][0]);
  e0[1] = feSub(feNeg(feMulCons(input[1][1], tatePower.xpowq)), feMulCons(input[1][2], tatePower.xpowq2));
  e0[1][0] = modSub(e0[1][0], input[1][0]);

  return lucasEven(fqMul(e1, fqInvert(fqMul(e0, input))), tatePower.phikonr);
};
tatePower.phikonr = str2bigInt("15028799613985034465755506450771569105982409691595259672696426485013");
tatePower.xpowq = strArray2fe([
  "657521270017796069346395138181635563647395275435524050257477505788",
  "6866761136669758413861832255861003032305625478275258872526688545352",
  "2015629970386633557539271384842840627883763896393847647101244719938"

]);
tatePower.xpowq2 = strArray2fe([
  "7680052371975202795937705303411295551244651607969534772653536375608",
  "11929925182031872553846797980141412727270917535480437433607306713575",
  "8162038477315276051893674194910562196977206739585131283469795294664"
]);

var tatePairing = function (P, Q) {
  var Z       = [P[0], P[1]],
      v       = [[one, zero, zero], [zero, zero, zero]],
      m_total = 224,
      bytes   = BigInt.bigInt2str(tatePairing.q, 2);
  Q = [feMul(Q[0], tatePairing.nqrinv), feMul(Q[1], tatePairing.nqrinv2)];

  var calculateTangent = function () {
    var temp1 = modMul(Z[0], Z[0]);
    temp1 = modMul(temp1, three);
    temp1 = modAdd(temp1, ecMultiplier);
    temp1 = modNeg(temp1);
    var temp2 = modAdd(Z[1], Z[1]);
    var temp3 = modMul(temp1, Z[0]);
    temp3 = modAdd(temp3, modMul(temp2, Z[1]));
    temp3 = modNeg(temp3);
    v = fqMul(v, dMillerEval(temp1, temp2, temp3, Q[0], Q[1]));
  };

  var calculateLine = function () {
    var temp1 = modSub(Z[1], P[1]);
    var temp2 = modSub(P[0], Z[0]);
    var temp3 = modMul(temp1, Z[0]);
    temp3 = modAdd(temp3, modMul(temp2, Z[1]));
    temp3 = modNeg(temp3);
    v = fqMul(v, dMillerEval(temp1, temp2, temp3, Q[0], Q[1]));
  };

  calculateTangent();
  for (var m = 222; m > 0; m--) {
    Z = ecDouble(Z);
    if (bytes.charAt(m_total - m - 1) == '1') {
      calculateLine();
      Z = ecAdd(Z, P);
    }
    v = fqMul(v, v);
    calculateTangent();
  }

  return tatePower(v);
};
tatePairing.nqrinv = [str2bigInt("6041968486146819522833566161674844805011295865114179686064601307785"), zero, zero];
tatePairing.nqrinv2 = [str2bigInt("12869220911900321812241940612176383961168937626526543351701949451131"), zero, zero];
tatePairing.q = str2bigInt("15028799613985034465755506450771561352583254744125520639296541195021");

/**
 * Calculate x^n. This method is slow.
 * @param {BigInt[][]} x
 * @param {BigInt} n
 * @returns {BigInt[][]}
 */
var g2Pow = function (x, n) {
  var result = [[zero, zero, zero], [zero, zero, zero]],
      lookup = new Array(lookupSize),
      bytes  = BigInt.bigInt2str(n, 2),
      i;

  // Build the lookup window, ignoring the first element.
  lookup[1] = [x[0], x[1]];
  for (i = 2; i < lookup.length; i++) {
    lookup[i] = ecFeAdd(lookup[i - 1], x);
  }

  var inWord = false,
      word   = 0,
      wBits  = 0;

  for (i = bytes.length - 1; i >= 0; i--) {
    var bit = (bytes.charAt(bytes.length - i - 1) == '1');
    result = ecFeDouble(result);

    if (!inWord && !bit) {
      continue;
    }

    if (!inWord) {
      inWord = true;
      word = wBits = 1;
    } else {
      word = word * 2 + (bit ? 1 : 0);
      wBits++;
    }

    if (wBits == 4 || i == 0) {
      result = ecFeAdd(result, lookup[word]);
      inWord = false;
    }
  }

  return result;
};

/**
 * Calculate x^n. This method is slow.
 * @param {BigInt[][]} x
 * @param {BigInt} n
 * @returns {BigInt[][]}
 */
var gtPow = function (x, n) {
  var result = [[one, zero, zero], [zero, zero, zero]],
      lookup = new Array(lookupSize),
      bytes  = BigInt.bigInt2str(n, 2),
      i;

  // Build the lookup window.
  lookup[0] = [[one, zero, zero], [zero, zero, zero]];
  for (i = 1; i < lookup.length; i++) {
    lookup[i] = fqMul(lookup[i - 1], x);
  }

  var inWord = false,
      word   = 0,
      wBits  = 0;

  for (i = bytes.length - 1; i >= 0; i--) {
    var bit = (bytes.charAt(bytes.length - i - 1) == '1');
    result = fqMul(result, result);

    if (!inWord && !bit) {
      continue;
    }

    if (!inWord) {
      inWord = true;
      word = wBits = 1;
    } else {
      word = word * 2 + (bit ? 1 : 0);
      wBits++;
    }

    if (wBits == 4 || i == 0) {
      result = fqMul(result, lookup[word]);
      inWord = false;
    }
  }

  return result;
};

/**
 * Create an IBE hash from a string. Uses SHA512, and truncates to the first 160 bits.
 * @param {string} input String to hash.
 * @returns {string} The IBE hash.
 */
var ibe_hash = function (input) {
  return crypto.createHash('sha512').update(input, 'utf8').digest().toString('hex').substring(0, 40);
};

/**
 * Get's an element of G1 from an id.
 * @param {string} id
 * @returns {BigInt[]}
 */
var g1FromId = function (id) {
  var s0 = ibe_hash(String.fromCharCode(1, 0, 0, 0, 0, 0, 0, 0) + id);
  var mcord = [6, 6, 2, 2, -2, -2, -6, -6];
  var s1 = "";
  for (var i = 0; i < s0.length; ++i) {
    s1 += s0[mcord[i % 8] + i];
  }

  var s = s1 + "00" + s1.substr(0, 14);
  var x = BigInt.str2bigInt(s, 16, 0);

  if (BigInt.greater(x, modulus)) {
    BigInt.rightShift_(x, 1);
  }

  var temp;
  while (true) {
    temp = modMul(x, x);
    temp = modAdd(temp, ecMultiplier);
    temp = modMul(temp, x);
    temp = modAdd(temp, g1FromId.multiplier);

    if (jacobi(temp, modulus) == 1) {
      break;
    }

    x = modMul(x, x);
    x = BigInt.addInt(x, 1);
  }

  var y = modSqrt(temp);
  y = modNeg(y);

  return [x, y];
};
g1FromId.multiplier = str2bigInt("9795501723343380547144152006776653149306466138012730640114125605701");

/**
 * Perform xor of two byte arrays.
 * @param {string} str
 * @param {string} key
 * @returns {string}
 */
var xor = function (str, key) {
  var encStr = "";
  for (var i = 0; i < str.length; i++) {
    encStr += String.fromCharCode(str.charCodeAt(i) ^ key.charCodeAt(i % key.length));
  }
  return encStr;
};

/**
 * A class for an IBE instantiation.
 * @constructor
 * @param {string} secret The IBE system's secret value. Must be set to generate private keys.
 * @param {BigInt[]} systemPublicKey The IBE system's public key. Set automatically if passing a secret.
 */
var IBE = function(secret, systemPublicKey) {

  /**
   * The IBE system's secret value.
   * @prop {BigInt}
   */
  this.secret = defaultKeyParams.secret;

  /**
   * The IBE system's public key. Derived from the system's secret.
   * @prop {BigInt[]}
   */
  this.systemPublicKey = defaultKeyParams.systemPublicKey;

  // Set values from the constructor arguments.
  if (secret) {
    this.secret = str2bigInt(secret);
    this.systemPublicKey = g2Pow(generator, this.secret);
  } else if (systemPublicKey) {
    this.secret = undefined;
    this.systemPublicKey = systemPublicKey;
  }
};

/**
 * Encrypt the given message with the public key for id.
 * @param {string} id Identity to encrypt this message for.
 * @param {string} msg Message to encrypt
 * @returns {{encMsg: String, U: BigInt[][]}}
 */
IBE.prototype.encrypt = function (id, msg) {
  var gt = tatePairing(g1FromId(id), this.systemPublicKey);
  var r = BigInt.randBigInt(223, 0);
  var U = g2Pow(generator, r);

  var enc = gtPow(gt, r);
  var encKey = ibe_hash(JSON.stringify(enc));
  var encMsg = new Buffer(xor(msg, encKey), 'utf8').toString('base64');

  return {
    encMsg: encMsg,
    U:      U
  };
};

/**
 * Decrypt the data with the given private key.
 * @param {BigInt[]} privateKey
 * @param {{encMsg: String, U: BigInt[][]}} data
 * @returns {string} The plaintext message from the data.
 */
IBE.prototype.decrypt = function (privateKey, data) {
  var gt = tatePairing(privateKey, data.U);
  var decKey = ibe_hash(JSON.stringify(gt));
  return xor(new Buffer(data.encMsg, 'base64').toString('utf8'), decKey);
};

/**
 * Generates the private key for ID
 * @param {string} id
 * @returns {BigInt[]} private key
 */
IBE.prototype.getPrivateKey = function (id) {
  var x      = g1FromId(id),
      result = [zero, zero],
      lookup = new Array(lookupSize),
      bytes  = BigInt.bigInt2str(this.secret, 2),
      i;

  // Build the lookup window.
  lookup[0] = [zero, zero];
  for (i = 1; i < lookupSize; i++) {
    lookup[i] = ecAdd(lookup[i - 1], x);
  }

  var inWord = false,
      word   = 0,
      wBits  = 0;

  for (i = bytes.length - 1; i >= 0; i--) {
    var bit = (bytes.charAt(bytes.length - i - 1) == '1');
    result = ecAdd(result, result);

    if (!inWord && !bit) {
      continue;
    }

    if (!inWord) {
      inWord = true;
      word = wBits = 1;
    } else {
      word = word * 2 + (bit ? 1 : 0);
      wBits++;
    }

    if (wBits == 4 || i == 0) {
      result = ecAdd(result, lookup[word]);
      inWord = false;
    }
  }

  return result;
};

module.exports = IBE;