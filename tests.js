/**
 * Test code for the ibe module.
 */

/**
 * Unit tests for the files route.
 * @module
 */

var _          = require('lodash'),
    assert     = require('chai').assert,
    IBE = require('./index');

describe('Testing IBE functionality.', function () {

  it('Test basic encryption', function testBigInt() {
    var ibe             = new IBE(),
        originalMessage = 'My message',
        id              = 'test',
        data            = ibe.encrypt(id, originalMessage),
        privateKey      = ibe.getPrivateKey(id),
        message         = ibe.decrypt(privateKey, data);

    assert.equal(message, originalMessage);
  });

  it('Test differing keys', function testBigInt() {
    var ibe             = new IBE(),
        originalMessage = 'My message',
        id              = 'test',
        id2             = 'test2',
        data            = ibe.encrypt(id, originalMessage),
        privateKey      = ibe.getPrivateKey(id2),
        message         = ibe.decrypt(privateKey, data);

    assert.notEqual(message, originalMessage);
  });

  it('Test new secret', function testBigInt() {
    var ibe             = new IBE('106810074544195281498118077145132002805729618501943234432491085370'),
        originalMessage = 'My message',
        id              = 'test',
        data            = ibe.encrypt(id, originalMessage),
        privateKey      = ibe.getPrivateKey(id),
        message         = ibe.decrypt(privateKey, data);

    assert.equal(message, originalMessage);
  });

  it('Test different secret', function testBigInt() {
    var ibe             = new IBE('106810074544195281498118077145132002805729618501943234432491085370'),
        originalMessage = 'My message',
        id              = 'test',
        data            = ibe.encrypt(id, originalMessage);

    var secondIBE  = new IBE('106810074544195923498118077145132002805729618501943234432491085370'),
        privateKey = secondIBE.getPrivateKey(id),
        message    = secondIBE.decrypt(privateKey, data);

    assert.notEqual(message, originalMessage);
  });

  it('Test setting public key', function testBigInt() {
    var ibe             = new IBE('106810074544195281498118077145132002805729618501943234432491085370'),
        originalMessage = 'My message',
        id              = 'test',
        privateKey      = ibe.getPrivateKey(id);

    var secondIBE = new IBE(null, ibe.systemPublicKey),
        data      = secondIBE.encrypt(id, originalMessage),
        message   = secondIBE.decrypt(privateKey, data);

    assert.equal(message, originalMessage);
  });

});