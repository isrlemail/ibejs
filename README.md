ibejs - Identity Based Encryption Javascript library.  
===

This library provides IBE functionality based on the Boneh-Franlink version of IBE using the Tate pairing.

The package provides a class that you can instantiate to use IBE.

The constructor takes two, mutually exclusive parameters:

1. secret. The system's secret, which is used to generate private keys. Will automatically set the systemPublicKey property.

2. systemPublicKey. The system's public key, which is used to generate public keys for identities. If set, will unset the secret.

The values for these properties is available as members of the instance object.

the IBE class also has the following methods:

* getPrivateKey(Id) -> PrivateKey
* encrypt(Id, Data) -> EncryptedData
* decrypt(PrivateKey, EncryptedData) -> Data

Id is an arbitrary string identifier. For example an email address. Data is a utf8 strings that hold whatever data is
to be encrypted. Generally, you will encrypt a symetric key, and then use this symetric key to encrypt longer
messages. EncryptedData and PrivateKey are both JavaScript objects that hold the necessary information.